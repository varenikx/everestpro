/**
 * @author alexandr [varenikx@gmail.com]
 */
define([
    'dojo/_base/declare',
    'dojo/dom-construct',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/text!./templates/menu.html',
    'dojo/window',
    'dojo/fx',
    'dojo/on',
    'dojox/gesture/swipe',
    'dojox/mobile/ListItem',
    'dojox/mobile/RoundRectList',
    'dojox/mobile/ComboBox',
    'dijit/form/DataList',
    'dojo/domReady!'
], function (declare, domConstruct, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin,
             template, win, fx, on, swipe) {

    /**
     * <p>Left menu.</p>
     *
     * @params {Object} param
     * @params {Boolean} param.swiped
     */
    var Class = declare([ _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin ], {

        class: 'slideMenu',

        templateString: template,

        /**
         * <p>If true,  display swipe will be controlled menu.</p>
         * @type {Boolean}
         */
        swiped: false,

        constructor: function () {
            _.bindAll(this, [
                '_onSwipe'
            ])
        },

        /**
         * <p>Show /hide menu.</p>
         * @param {Boolean} state
         */
        show: function (state) {
            this._isOpen = state

            fx.slideTo(
                {
                    node: this.domNode,
                    left: (state) ? 0 : (0 - this._displaySize.w)
                }).play()
        },

        /* Protected */

        buildRendering: function () {

            this.inherited(arguments)
            this._calculateDisplay()

            this.domNode.style.left = (0 - this._displaySize.w) + 'px'

            on(document.body, swipe, this._onSwipe)
        },

        resize: function () {
            this._calculateDisplay()
            this.inherited(arguments)
        },

        /* Private */

        _isOpen: false,


        /**
         * <p>DisplaySize.</p>
         * @type {Object}
         */
        _displaySize: null,

        /**
         * <p>on Device swap.</p>
         * @param {Object} swipe
         * @param {String|Number} swipe.dx
         * @param {String|Number} swipe.dy
         * @param {String|Number} swipe.time
         *
         * @private
         */
        _onSwipe: function (swipe) {
            if (this.swiped) {
                if (swipe.time > 100 && (Math.abs(swipe.dx) > Math.abs(swipe.dy))) {
                    // this horizontal swipe
                    if (swipe.dx > 0 && !this._isOpen) {
                        // left > right swipe
                        // open if menu closed
                        this.show(true)
                    }
                    if (swipe.dx < 0 && this._isOpen) {
                        // right > left swipe
                        // close if menu opened
                        this.show(false)
                    }

                }
            }
        },

        /**
         * <p>Get and set actual display size.</p>
         * @private
         */
        _calculateDisplay: function () {
            this._displaySize = win.getBox()
        }
    })
    return Class
})
