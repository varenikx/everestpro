/**
 * @author alexandr [varenikx@gmail.com]
 */
define([
    'dojo/_base/declare',
    'dojox/mobile/View',
    'dijit/_TemplatedMixin',
    "dojo/text!./templates/app.html",
    'dojo/_base/fx',
    'dojox/gesture/tap',
    'dojo/on'

],function (declare, View, _TemplatedMixin, template, fx, tap, on) {

    var Class = declare([ View, _TemplatedMixin ], {

        'class': 'screen appScreen',

        templateString: template,

        constructor: function () {
            _.bindAll(this, [
                '_showOnTap'
            ])
        },

        /**
         * <p>Shiw/hide FAB.</p>
         * @param {Boolean} state
         */
        showFab: function (state) {
            if (state) {
                fx.fadeIn({
                    node: this.fab,
                    duration: 200
                }).play();
            } else {
                fx.fadeOut({
                    node: this.fab,
                    duration: 500
                }).play();
            }
        },

        /* Protected */

        buildRendering: function () {
            this.inherited(arguments)

            // slow hide fab
            setTimeout(function () {
                this.showFab(false)
            }.bind(this), 2000)

            on(this.domNode, tap, this._showOnTap)
        },

        /* Private */

        _showOnTap: function () {
            this.showFab(true)

            setTimeout(function () {
                this.showFab(false)
            }.bind(this), 2000)
        }

    })
    return Class
})
