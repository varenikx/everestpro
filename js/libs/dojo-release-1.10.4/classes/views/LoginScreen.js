/**
 * @author alexandr [varenikx@gmail.com]
 */
define([
    'dojo/_base/declare',
    'dojox/mobile/View',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/text!./templates/login.html',
    'dojox/gesture/tap',
    'dojo/dom-class',
    'dojo/fx',
    'dojox/gesture/swipe',
    'dojo/on'

],function (declare, View, _TemplatedMixin, _WidgetsInTemplateMixin,
            template, tap, domClass, fx, swipe, on) {

    var Class = declare([ View, _TemplatedMixin, _WidgetsInTemplateMixin], {

        'class': 'screen loginScreen',

        templateString: template,

        /**
         * @event
         */
        onLogin: function () {},

        /**
         * <p>Show/hide screen.</p>
         * @param {Boolean} state
         */
        show: function (state) {
            this.domNode.style.display = (state)
                ? 'block'
                : 'none'
        },

        constructor: function () {
            _.bindAll(this, [
                '_onSingInTap'
            ])
        },

        /* Protected */

        buildRendering: function () {
            this.inherited(arguments)

            on(this.singIn, tap, this._onSingInTap)
        },

        /* Private */

        /**
         * <p>Login in app.</p>
         * @params {Object} params
         * @params {Object} params.login
         * @params {Object} params.password
         * @private
         */
        _login: function (params) {
            /*FIXME по условию логин с фиксированными данными*/
            var stateLogin = false, statePassword = false
            if (params.login == 'EverestPro') {
                stateLogin = true
            }
            if (params.password == '123') {
                statePassword = true
            }
            if (stateLogin && statePassword) {
                this.onLogin()
            } else {
                // invalid login
                domClass.add(this.loginInput.domNode, "invalid")
                domClass.add(this.passwordInput.domNode, "invalid")

                this._shiverNode(this.domNode)
            }
        },

        /**
         * <p>On singIn tap.</p>
         * @private
         */
        _onSingInTap: function () {
            domClass.remove(this.loginInput.domNode, "invalid")
            domClass.remove(this.passwordInput.domNode, "invalid")
            this._login({
                login: this.loginInput.get('value'),
                password: this.passwordInput.get('value')
            })
        },

        /**
         * <p>Shiver node.</p>
         * @param {HTMLElement} node
         * @private
         */
        _shiverNode: function (node) {
            var anim1 = fx.slideTo({
                duration: 20,
                node: node,
                left: -10
            })
            var anim2 = fx.slideTo({
                duration: 20,
                node: node,
                left: 10
            })

            var anim3 = fx.slideTo({
                duration: 20,
                node: node,
                left: -9
            })

            var anim4 = fx.slideTo({
                duration: 20,
                node: node,
                left: 8
            })

            var anim5 = fx.slideTo({
                duration: 20,
                node: node,
                left: 0
            })

            var slides = fx.chain([anim1, anim2, anim3, anim4, anim5])
            slides.play()
        }
    })
    return Class
})
